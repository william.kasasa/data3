import os
os.chdir(r'E:\kdg\Data 3\data_aux')
os.environ["PATH"] += os.pathsep + 'C:/Program Files/Graphviz/bin/'
import numpy as np
import pandas as pd
from IPython.display import display
import math
import matplotlib
import matplotlib.pyplot as plt
import statistics
import csv
import graphviz
import six
import sys
sys.modules['sklearn.externals.six'] = six
#from id3 import Id3Estimator, export_graphviz, export_text
from scipy import stats
import scipy.spatial.distance as dist
from scipy.spatial.distance import cdist
from scipy.cluster.hierarchy import linkage, dendrogram, cut_tree, linkage
from sklearn import tree
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import DistanceMetric
from sklearn.cluster import KMeans, AgglomerativeClustering
from sklearn.metrics import accuracy_score, precision_recall_fscore_support
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve, roc_auc_score

from tensorflow import keras
from tensorflow.keras import Model
from tensorflow.keras.layers import Input, Dense
from tensorflow.keras.utils import to_categorical
from tensorflow. keras.optimizers import Adam, RMSprop
from livelossplot import PlotLossesKeras
from keras.utils.vis_utils import plot_model
from sklearn.model_selection import train_test_split

pd.set_option('display.max_columns', None)
pd.set_option('display.max_row', None)

missing_values = ['n/a', 'na', 'nan', 'N/A', 'NA', 'NaN', 'NAN', '--', 'Missing', 'missing', 'Unknown', 'unknown', 'UNKNOWN']

"""Home made functions - Data & AI 2"""
def all_freq(x):
    # index= x.unique()
    t_abs = x.value_counts(dropna=False).sort_index()
    t_rel = (x.value_counts(dropna=False,normalize=True).sort_index()*100).round(1)
    t_abs_cum = x.value_counts(dropna=False).sort_index().cumsum()
    t_rel_cum = (x.value_counts(dropna=False,normalize=True).sort_index().cumsum()*100).round(1)
    return pd.DataFrame({'abs freq':t_abs,'rel freq':t_rel,'abs cum freq':t_abs_cum,'rel cum freq':t_rel_cum}) #,index=index)


"""Home made functions - Data & AI 3"""

def plotbinom (n, p):
    x = range(0, n+1)
    plt.figure()
    fig, ax = plt.subplots(1, 1)
    ax.plot(x, binom.pmf(x, n, p), 'bo', ms=0)
    ax.vlines(x, 0, binom.pmf(x, n, p), colors='b', lw=15)
    plt.show()
    return

#Generate csv file with a given number of random datapoints but with a given mean and std
def make_csv (n,mu,sd,name_file):
   array01=np.random.normal(mu, sd, size=n)
   df = pd.DataFrame({'Random': array01})
   df['Stand'] = (df['Random'] - df['Random'].mean())/df['Random'].std()
   df[name_file] = (df['Stand'] * sd) + mu
   df[name_file].to_csv(name_file+'.csv',index=False)
   return

make_csv(30,40,20,'consumptionLaptops')

""" Evaluation Metrics"""
# Visualisation of the confusion table
def show_confusion_table(conf_matrix, labels):
    fig, ax = plt.subplots(figsize=(5, 5))
    ax.matshow(conf_matrix, cmap=plt.cm.Oranges, alpha=0.3)
    for i in range(conf_matrix.shape[0]):
        for j in range(conf_matrix.shape[1]):
            ax.text(x=j, y=i,s=conf_matrix[i, j], va='center', ha='center', size='xx-large')
    plt.xlabel('Predictions', fontsize=18)
    plt.ylabel('Actuals', fontsize=18)
    plt.title('Confusion Matrix', fontsize=18)
    ax.set_xticks(range(0,len(labels)))
    ax.set_xticklabels(labels)
    ax.set_yticks(range(0,len(labels)))
    ax.set_yticklabels(labels)
    plt.show()

#Plot the ROC curve
def plot_roc(y_true, y_score, title='ROC Curve', **kwargs):
    title='ROC Curve'
    if 'pos_label' in kwargs:
        fpr, tpr, thresholds = roc_curve(y_true=y_true, y_score=y_score, pos_label=kwargs.get('pos_label'))
        auc = roc_auc_score(y_true, y_score)
    else:
        fpr, tpr, thresholds = roc_curve(y_true=y_true, y_score=y_score)
        auc = roc_auc_score(y_true, y_score)

    # calculate optimal cut-off with Youden index method
    optimal_idx = np.argmax(tpr - fpr)
    optimal_threshold = thresholds[optimal_idx]

    figsize = kwargs.get('figsize', (7, 7))
    fig, ax = plt.subplots(1, 1, figsize=figsize)
    ax.grid(linestyle='--')

    # plot ROC curve
    ax.plot(fpr, tpr, color='darkorange', label='AUC: {}'.format(auc))
    ax.set_title(title)
    ax.set_xlabel('False Positive Rate (FPR)')
    ax.set_ylabel('True Positive Rate (TPR)')
    ax.fill_between(fpr, tpr, alpha=0.3, color='darkorange', edgecolor='black')

    # plot classifier
    ax.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')

    # plot optimal cut-off
    ax.scatter(fpr[optimal_idx], tpr[optimal_idx], label='optimal cutoff {:.2f} on ({:.2f},{:.2f})'.format(optimal_threshold, fpr[optimal_idx], tpr[optimal_idx]), color='red')
    ax.plot([fpr[optimal_idx], fpr[optimal_idx]], [0, tpr[optimal_idx]], linestyle='--', color='red')
    ax.plot([0, fpr[optimal_idx]], [tpr[optimal_idx], tpr[optimal_idx]], linestyle='--', color='red')

    ax.legend(loc='lower right')
    plt.savefig("myROCcurve.png")
    plt.show()

# when not the raw data but a confusion matrix is given
def trueFalse (confusion_matrix):
    TP = confusion_matrix.iloc[0][0]
    print('TP', TP)
    TN = np.diag(confusion_matrix).sum() - TP
    print('TN:', TN)
    FP = confusion_matrix.iloc[:, 0].sum() - TP
    print('FP:', FP)
    FN = confusion_matrix.sum().sum() - np.diag(confusion_matrix).sum() - FP
    print('FN:', FN)
    return

def accuracy(confusion_matrix):
    return np.diag(confusion_matrix).sum()/confusion_matrix.sum().sum()

def precision(confusion_matrix):
    precision = []
    n = confusion_matrix.shape[1]
    for i in range(0,n):
        TP = confusion_matrix.loc[confusion_matrix.index[i]][confusion_matrix.index[i]]
        precision = precision + [TP/confusion_matrix.loc[:, confusion_matrix.index[i]].sum()]
    return precision

def recall(confusion_matrix):
    recall = []
    n = confusion_matrix.shape[0]
    for i in range(0,n):
        TP = confusion_matrix.loc[confusion_matrix.index[i]][confusion_matrix.index[i]]
        recall = recall + [TP/confusion_matrix.loc[confusion_matrix.index[i], :].sum()]
    return recall

def f_measure(confusion_matrix, beta):
    precisionarray = precision(confusion_matrix)
    recallarray = recall(confusion_matrix)
    fmeasure=[]
    n = len(precisionarray)
    for i in range(0,n):
        p = precisionarray[i]
        r = recallarray [i]
        fmeasure = fmeasure + [((beta*beta+1)*p*r)/(beta*beta*p+r)]
    return fmeasure

def overviewmetrics(confusion_matrix, beta):
    overview_1 = np.transpose(precision (confusion_matrix))
    overview_2 = np.transpose(recall(confusion_matrix))
    overview_3 = np.transpose(f_measure(confusion_matrix,beta))
    overview_table=pd.DataFrame (data=np.array([overview_1, overview_2, overview_3]), columns=confusion_matrix.index)
    overview_table.index = ['precision', 'recall', 'fx']
    return[overview_table]

def positiverates(confusion_matrix):
    if (confusion_matrix.shape[0] == 2) & (confusion_matrix.shape[1] == 2):
        TPR = confusion_matrix.iloc[0][0]/confusion_matrix.iloc[0, :].sum()
        print('TPR', TPR)
        FPR = confusion_matrix.iloc[1][0]/confusion_matrix.iloc[1, :].sum()
        print('FPR', FPR)
    return

def positiverates2(confusion_matrix):  #to be tested ;-)
    if (confusion_matrix.shape[0] == 2) & (confusion_matrix.shape[1] == 2):
        TPR = confusion_matrix.loc[confusion_matrix[0]][confusion_matrix[0]]/confusion_matrix.loc[confusion_matrix[0], :].sum()
        print('TPR', TPR)
        FPR = confusion_matrix.loc[confusion_matrix[1]][confusion_matrix[0]]/confusion_matrix.loc[confusion_matrix[1], :].sum()
        print('FPR', FPR)
    return

"""EXTRA: what if crosstab does not generate a square confusion matrix?"""
def square_conf_mat (conf_mat):
    if (conf_mat.index.size > conf_mat.columns.size):
        names = conf_mat.index
    else:
        names = conf_mat.columns
    col_names = names.copy()
    col_names.name = 'predicted'
    row_names = names.copy()
    row_names.name = 'actual'
    data=np.zeros((names.size,names.size), dtype=int)
    new_df = pd.DataFrame(data=data, index=row_names, columns=col_names)
    for j in conf_mat.columns:
        for i in conf_mat.index:
            new_df.loc[i][j] = conf_mat.loc[i][j]
    return(new_df)

"""Discriminant analysis"""
def LDA_coefficients(X,lda):
    nb_col = X.shape[1]
    matrix= np.zeros((nb_col+1,nb_col), dtype=int)
    Z=pd.DataFrame(data=matrix,columns=X.columns)
    for j in range(0,nb_col):
        Z.iloc[j,j] = 1
    LD = lda.transform(Z)
    nb_funct= LD.shape[1]
    result = pd.DataFrame();
    index = ['const']
    for j in range(0,LD.shape[0]-1):
        index = np.append(index,'C'+str(j+1))
    for i in range(0,LD.shape[1]):
        coef = [LD[-1][i]]
        for j in range(0,LD.shape[0]-1):
            coef = np.append(coef,LD[j][i]-LD[-1][i])
        coef_column = pd.Series(coef)
        coef_column.index = index
        column_name = 'LD' + str(i+1)
        result[column_name] = coef_column
    return result

"""Neural Networks"""

###Normalisation###

def Zscore_norm(col):
    mean = col.mean()
    std = col.std()
    return (col-mean)/std

def min_max_norm(col):
    minimum = col.min()
    range = col.max() - minimum
    return (col-minimum)/range

def decimal_scaling_norm(col):
    maximum = col.max()
    tenfold = 1
    while (maximum > tenfold):
        tenfold = tenfold *10
    return (col/tenfold)

def tanh_norm(col):
    mean = col.mean()
    std = col.std()
    n = len(col)
    return(0.5 * np.tanh(1 + 0.01 * ((col-mean)/(std / math.sqrt(n)))))

def median_mad_norm(col):
    # do not use if 50% or more of the data have the same value
    median = col.median()
    mad = (abs(col - median).median())
    return ((col-median)/mad)


def normalize_values(df, norm_func):
    df_norm = pd.DataFrame()
    for column in df:
        df_norm[column] = norm_func(df[column])
    return df_norm

#test1 = normalize_values(df,Zscore_norm)
#test2 = normalize_values(df,min_max_norm)
#test3 = normalize_values(df,decimal_scaling_norm)
#test4 = normalize_values(df,tanh_norm)
#test5 = normalize_values(df,median_mad_norm)